﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace PageObjectsWithNUnit.Pages
{
    public class BasicDropbox
    {

        private string url = "https://www.seleniumeasy.com/test/basic-select-dropdown-demo.html";

        private IWebDriver driver;

        public BasicDropbox(IWebDriver driver)
        {
            this.driver = driver;

            PageFactory.InitElements(driver, this);
        }
        
        [FindsBy(How = How.Id, Using = "select-demo")]
        private IWebElement dropBox;

        [FindsBy(How = How.ClassName, Using = "selected-value")]
        private IWebElement message;

       
        public void GoToPage()
        {
            driver.Navigate().GoToUrl(url);
        }

        public void Dropbox()
        {
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", dropBox);
            dropBox.Click();
            driver.FindElement(By.XPath("//*[text()='Sunday']")).Click();
            dropBox.SendKeys(Keys.ArrowDown);
            dropBox.SendKeys(Keys.Enter);

        }

        public string GetMessage()
        {
            return message.Text;
        }
    }
}
