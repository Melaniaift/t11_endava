﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using System.Threading;

namespace PageObjectsWithNUnit.Pages
{
    public class MultiSelect
    {
        private string url = "https://www.seleniumeasy.com/test/basic-select-dropdown-demo.html";
        private IWebDriver driver;

        public MultiSelect(IWebDriver driver)
        {
            this.driver = driver;

            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.Id, Using = "multi-select")]
        private IWebElement multiselect;

        [FindsBy(How = How.ClassName, Using = "getall-selected")]
        private IWebElement message;

        [FindsBy(How = How.Id, Using = "printAll")]
        private IWebElement buttonPrintAll;

        //[FindsBy(How = How.Name, Using = "States")]
        //private SelectElement oSelection;
        public void GoToPage()
        {
            driver.Navigate().GoToUrl(url);
        }

        public void MultiSelect_()
        {
            SelectElement oSelection = new SelectElement(driver.FindElement(By.Name("States")));
            
            //multiselect.(Keys.Control);
            oSelection.SelectByIndex(2);
            //multiselect.SendKeys(Keys.Control);
            oSelection.SelectByIndex(3);
            //multiselect.SendKeys(Keys.Control);
            buttonPrintAll.Click();
            Thread.Sleep(2000);

            

            
        }

        public string GetMessage()
        {
            return message.Text;
        }
    }
}
