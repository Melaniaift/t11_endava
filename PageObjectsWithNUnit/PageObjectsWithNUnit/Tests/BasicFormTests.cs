﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using PageObjectsWithNUnit.Pages;

namespace PageObjectsWithNUnit.Tests
{
    [TestFixture]
    public class BasicFormTests
    {
        IWebDriver driver;
        BasicForm basicForm;
        BasicCheckbox basicCheckbox;
        BasicDropbox basicDropbox;
        MultiSelect multiSelect;

        [SetUp]
        public void Init()
        {
            driver = new ChromeDriver();
            driver.Manage().Cookies.DeleteAllCookies();

            basicForm = new BasicForm(driver);
            basicCheckbox = new BasicCheckbox(driver);
            basicDropbox = new BasicDropbox(driver);
            multiSelect = new MultiSelect(driver);
        }

        [Test]
        public void ValidateMessageIsDisplayedWhenUserEntersTextAndClickShowMessage()
        {
            // Arrange
            string message = "abcd";

            basicForm.GoToPage();
            basicForm.EnterMessage(message);

            // Act
            basicForm.ClickShowMessage();

            // Assert
            Assert.That(basicForm.GetDisplayedMessage(), Is.EqualTo(message));
        }

        [Test]
        public void ValidateMessageIsDisplayedWhenUserEntersTextAndClickShowMessageNoPageObjects()
        {
            // Arrange
            string message = "abcd";

            driver.Navigate().GoToUrl("https://www.seleniumeasy.com/test/basic-first-form-demo.html");
            driver.FindElement(By.Id("user-message")).SendKeys(message);

            // Act
            driver.FindElement(By.CssSelector("form[id='get-input'] > button")).Click();

            // Assert
            Assert.That(driver.FindElement(By.Id("display")).Text, Is.EqualTo(message));
        }

        [Test]
        public void ValidateCheckBox()
        {

            // Arrange
            basicCheckbox.GoToPage();

            // Act
            basicCheckbox.CheckBox();

            // Assert
            Assert.That(basicCheckbox.GetMessage(), Is.EqualTo("Success - Check box is checked"));
        }

        [Test]
        public void ValidateDropdown()
        {

            // Arrange
            basicDropbox.GoToPage();

            // Act
            basicDropbox.Dropbox();

            // Assert
            Assert.That(basicDropbox.GetMessage(), Is.EqualTo("Day selected :- Sunday"));
        }

        [Test]
        public void MultiSelect_()
        {

            // Arrange
            multiSelect.GoToPage();
            
            // Act
            multiSelect.MultiSelect_();

            // Assert
            Assert.That(multiSelect.GetMessage(), Is.EqualTo("Options selected are : New York"));
        }
        

        [TearDown]
        public void Cleanup()
        {
            driver.Quit();
        }
    }
}
