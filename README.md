# T11_endava

PageObjects:

- [ ] Create a page object for https://www.seleniumeasy.com/test/basic-select-dropdown-demo.html web page and write some tests that will cover single and multiple dropdown selections
- [ ] Create a page object for https://www.seleniumeasy.com/test/table-search-filter-demo.html web page and write few tests that validates search filter
- [ ] Create a page object for https://www.seleniumeasy.com/test/table-pagination-demo.html web page and write few tests that will validate pagination (check on google how to handle a table in selenium web driver)
- [ ] Create a page object for https://www.seleniumeasy.com/test/jquery-download-progress-bar-demo.html web page and write a test that will validate download is complete (use fluent wait when waiting for progress bar to complete, don’t use specific time)     
 



For those who don’t have access to gitlab repo I’ve attached project zip file. Please send the homework to HR: Ioana.Sescu@endava.com , Claudia.Vasiliu@endava.com and add Elena.Marciuc@endava.com in CC

 

 

Git commands:


- git status (info about the branch)
- git log ( view commits)
- git pull (sync local branch with the origin one)
- git push (publish a local branch on origin)
- git checkout branch_name (switch to branch branch_name)
- git checkout -b new_branch ( creates a new branch locally)
- git add . (stage all changed or new files)
- git add specific_file (stage a specific file)
- git commit -m “commit message” (add a commit for the changes you made)
- git merge main branch_name ( sync branch branch_name with main branch) this command is used when main is updated and your branch branch_name is behind main with a list of changes)
